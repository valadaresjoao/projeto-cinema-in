const express = require("express")
const app = express()

const movieRoute = require("./src/routes/cineMovieRoute")
const userRoute = require("./src/routes/userRoute");
const sessionRouter = require("./src/routes/sessionsRoute")
const seatRouter = require("./src/routes/seatsRoute")
const { createDatabase } = require("./src/database/createData");

const cors = require("cors");

const port = 8000;
console.log("AAAAAA");
app.use(cors());
app.use(express.json());

app.use("/movie", movieRoute);
app.use("/user", userRoute);
app.use("/session", sessionRouter);
app.use("/seats", seatRouter);

app.listen(port, ()=>{
    console.log("Servewr on fire!!!!!!")
})

//  try {
//     createDatabase()   
//     } catch (error) {
//      console.log(error)
// }

