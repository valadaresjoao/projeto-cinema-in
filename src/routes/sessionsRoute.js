const express = require("express");
const router = express.Router();

const {
  postSession,
  getSession,
  getAllSessions,
  fiterMoviesSection,
} = require("../controllers/sessionController");

router.post("/:id", (req, res) => {
  postSession(req, res);
});

router.get("/:id", (req, res) => {
  getAllSessions(req, res);
});

router.post("/search/:id", (req, res) => {
  fiterMoviesSection(req, res);
});
  

// Aqui ambas as rotas eram com a mesma rota e o mesmo método, nunca que iria chegar nessa rota, então 
// coloquei o 1 id(filme) e o 2(sessao) para pegar uma sessão específica de um filme
router.get("/:id/:sessaoId", (req, res) => {
    getSession(req, res);
});

module.exports = router;


  