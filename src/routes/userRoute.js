const express = require("express");
const router = express.Router();

const { postUser, validateUser, allUsers,  findUser} = require("../controllers/userController")

router.post("/cadastro", (req, res) => {
  postUser(req, res);
});

router.get("/login", (req, res) => {
  validateUser(req, res);
});

router.get("/:nome", (req, res) => {
  findUser(req, res);
});

router.get("/", (req, res) => {
  allUsers(req, res);
});

module.exports = router;
