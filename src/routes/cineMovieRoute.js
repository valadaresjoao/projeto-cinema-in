const express = require("express");
const router = express.Router();

const {
  postMovie,
  getMovie,
  getAllMovies,
  deleteMovie,
  searchMovies,
} = require("../controllers/movieController");

router.post("/search", (req, res) => {
  searchMovies(req, res);
});

router.post("/", (req, res) => {
  postMovie(req, res);
});

router.get("/", (req, res) => {
  getAllMovies(req, res);
});

router.get("/:id", (req, res) => {
  getMovie(req, res);
});

router.delete("/:id", (req, res) => {
  deleteMovie(req, res);
});



module.exports = router;