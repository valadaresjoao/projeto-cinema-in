const express = require("express");
const router = express.Router();

const {
    getSeat,
    getAllSeats,
    patchSeat
} = require("../controllers/seatsController")


router.get("/:sessionId/:seatId", (req,res) => {
    getSeat(req,res);
})

router.get("/:seats", (req,res) => {
    getAllSeats(req,res);
})

router.patch("/:sessionId/:seatId", (req,res) => {
    patchSeat(req,res);
})


module.exports = router;