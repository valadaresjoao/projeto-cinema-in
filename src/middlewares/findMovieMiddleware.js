const { Filmes, Sessoes, Assentos ,Usuarios } = require("../models/cineMainModel");

function findMovie(req, res, next){

    const { id } = req.params.movieid;
    const movie = Filmes.findByPk(id);

    if(!movie){
        return res.status(404).json({error: "Movie not found"})
    }

    req.movie = movie;

    console.log(movie)

    return next();
}

module.exports = findMovie;