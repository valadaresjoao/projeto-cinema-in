const { sequelize } = require("../connection/createConfigData");
const { Filmes, Sessoes, Usuarios } = require("../models/cineMainModel");
const { Op } = require('sequelize')

module.exports = {
  postMovie: async (req, res) => {
    try {
      const movies = await Filmes.create({
        titulo: req.body.titulo,
        direcao: req.body.direcao,
        urlImagem: req.body.urlImagem,
        sinopse: req.body.sinopse,
        genero: req.body.genero,
        classificacao: req.body.classificacao,
      });

      return res.status(201).json(movies);
    } catch (error) {
      return res.status(500).json(error);
    }
  },

  getMovie: async (req, res) => {
    try {
      const movie = await Filmes.findByPk(req.params.id);
      if (movie) {
        return res.status(200).json(movie);
      } else {
        return res.status(500).json({ error: "Could not find the movie " });
      }
    } catch (error) {
      return res.status(500).json(error);
    }
  },

  getAllMovies: async (req, res) => {
    try {
      const movies = await Filmes.findAll({});
      if (movies) {
        return res.status(200).json(movies);
      } else {
        return res.status(501).json({ error: "Your database has no movies" });
      }
    } catch (error) {
      return res.status(501).json(error);
    }
  },

  deleteMovie: async (req, res) => {
    try {
      const movie = await Filmes.destroy({
        where: {
          id: req.params.id,
        },
      });

      if (!movie) {
        return res.status(500).json({ error: "Could not find the movie " });
      }

      return res.status(200).json(movie);
    } catch (error) {
      return res.status(500).json(error);
    }
  },
  searchMovies: async (req, res) => {
    console.log(req.body.titulo);
    console.log(req.body.genero);
    console.log(req.body.classificacao);

    

    if (!req.body.titulo && !req.body.genero && !req.body.classificacao){
      const allMovies = await Filmes.findAll({});

      return res.status(200).json(allMovies);
    }

        // Verificar se o usuário colocou o nome do filme, genero e classifricação, mudando
    // o filtro dependendo do que o usuário colocou
    
    if (
      Boolean(req.body.titulo && req.body.genero && req.body.classificacao >= 0)
    ) {
      console.log("1");
      try {
        const resultsWithGenreAndClassificationMovies = await Filmes.findAll({
          where: {
            titulo: req.body.titulo,
            genero: req.body.genero,
            classificacao: {
              [Op.lte]: req.body.classificacao,
            },
          },
        });

        return res.status(200).json(resultsWithGenreAndClassificationMovies);
      } catch (error) {
        console.log(error);
      }
    }

    if (req.body.titulo && req.body.genero) {
      console.log("2");
      try {
        const resultsWithGenreMovies = await Filmes.findAll({
          where: {
            titulo: req.body.titulo,
            genero: req.body.genero,
          },
        });

        return res.status(200).json(resultsWithGenreMovies);
      } catch (error) {
        console.log(error);
      }
    }
    if (req.body.titulo && req.body.classificacao) {
      console.log("3");
      try {
        const resultsWithClassificationMovies = await Filmes.findAll({
          where: {
            titulo: req.body.titulo,
            classificacao: {
              [Op.lte]: req.body.classificacao,
            },
          },
        });

        return res.status(200).json(resultsWithClassificationMovies);
      } catch (error) {
        console.log(error);
      }
    }

    if (req.body.genero && req.body.classificacao) {
      console.log("4");
      try {
        const resultsWithClassificationMovies = await Filmes.findAll({
          where: {
            genero: req.body.genero,
            classificacao: {
              [Op.lte]: req.body.classificacao,
            },
          },
        });

        return res.status(200).json(resultsWithClassificationMovies);
      } catch (error) {
        console.log(error);
      }
    }


    if (req.body.titulo) {
      console.log("5");
      try {
        const resultsJustWithMovies = await Filmes.findAll({
          where: {
            titulo: req.body.titulo,
          },
        });

        return res.status(200).json(resultsJustWithMovies);
      } catch (error) {
        console.log(error);
      }
    }

    if (req.body.genero) {
      console.log("6");
      try {
        const resultsJustWithGender = await Filmes.findAll({
          where: {
            genero: req.body.genero,
          },
        });

        return res.status(200).json(resultsJustWithGender);
      } catch (error) {
        console.log(error);
      }
    }

    if (req.body.classificacao >= 0) {
      console.log("7");
      try {
        const resultsJustWithClassification = await Filmes.findAll({
          where: {
            classificacao: {
              [Op.lte]: req.body.classificacao,
            },
          },
        });

        return res.status(200).json(resultsJustWithClassification);
      } catch (error) {
        console.log(error);
      }
    }

    return res.status(400).json({ error: "nenhum filme achado na pesquisa" });
  }
};
