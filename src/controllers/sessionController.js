const { Filmes, Sessoes, Assentos ,Usuarios } = require("../models/cineMainModel");
const { Op } = require("sequelize");

module.exports = {
  postSession: async (req, res) => {
    try {
      const sessions = await Sessoes.create({
        horario: req.body.horario,
        cidade: req.body.cidade,
        bairro: req.body.bairro,
        tipo: req.body.tipo,
        cineminha: req.params.id,
      });

      //Criação dos assentos
      let fileira = 1;
      while (fileira <= 10) {
        let assento = 1;
        while (assento <= 18) {
          const assentos = await Assentos.create({
            numero: assento,
            fileira: fileira,
            sessionId: sessions.id,
            tipoDoAssento: req.body.tipoDoAssento,
            preco: req.body.preco,
          });
          assento = assento + 1;
        }
        fileira = fileira + 1;
      }

      return res.status(201).json(sessions);
    } catch (error) {
      return res.status(501).json(error);
    }
  },
  getSession: async (req, res) => {
    try {
      const sessaoFilme = await Sessoes.findOne({
        where: {
          cineminha: req.params.id,
          id: req.params.sessaoId,
        },
      });

      if (sessaoFilme) {
        res.status(200).json(sessaoFilme);
      } else {
        res.status(400).json({ error: "Esse filme não tem essa sessão" });
      }
    } catch (error) {}
  },
  getAllSessions: async (req, res) => {
    try {
      const sessions = await Sessoes.findAll({
        where: {
          cineminha: req.params.id,
        },
      });
      return res.status(200).json(sessions);
    } catch (error) {
      return res.status(500).json(error);
    }
  },

  fiterMoviesSection: async (req, res) => {
    console.log(req.body.cidade);
    console.log(req.body.bairro);
    console.log(req.body.tipo);
    console.log(req.params.id);

    // Verificar se o usuário colocou o nome do filme, genero e classifricação, mudando
    // o filtro dependendo do que o usuário colocou

    if (req.body.cidade && req.body.bairro && req.body.tipo >= 0) {
      console.log("1");
      try {
        const resultsWithCityAndBairroAndType = await Sessoes.findAll({
          where: {
            cineminha: req.params.id,
            bairro: req.body.bairro,
            tipo: {
              [Op.eq]: req.body.tipo,
            },
          },
        });

        return res.status(200).json(resultsWithCityAndBairroAndType);
      } catch (error) {
        console.log(error);
      }
    }

    if (req.body.cidade && req.body.bairro) {
      console.log("2");

      try {
        const resultsWithCityAndBairro = await Sessoes.findAll({
          where: {
            cidade: req.body.cidade,
            bairro: req.body.bairro,
            cineminha: req.params.id,
          },
        });

        return res.status(200).json(resultsWithCityAndBairro);
      } catch (error) {
        console.log(error);
      }
    }

    if (req.body.tipo >= 0 && req.body.cidade) {
      console.log("3");

      try {
        const resultsJustWithType = await Sessoes.findAll({
          where: {
            tipo: {
              [Op.eq]: req.body.tipo,
            },
            cidade: req.body.cidade,
            cineminha: req.params.id,
          },
        });

        if (resultsJustWithType.length !== 0) {
          return res.status(200).json(resultsJustWithType);
        }
      } catch (error) {
        console.log(error);
      }
    }

    if (req.body.bairro && req.body.tipo >= 0) {
      console.log("4");

      try {
        const resultsJustWithMovies = await Sessoes.findAll({
          where: {
            cineminha: req.params.id,
            tipo: req.body.tipo,
            bairro: req.body.bairro,
          },
        });

        if (resultsJustWithMovies.length !== 0) {
          return res.status(200).json(resultsJustWithMovies);
        }
      } catch (error) {
        console.log(error);
      }
    }
    // ------------------------------------------------------------------------

    if (req.body.tipo >= 0) {
      console.log("4");

      try {
        const resultsJustWithMovies = await Sessoes.findAll({
          where: {
            cineminha: req.params.id,
            tipo: req.body.tipo,
          },
        });

        if (resultsJustWithMovies.length !== 0) {
          return res.status(200).json(resultsJustWithMovies);
        }
      } catch (error) {
        console.log(error);
      }
    }
    // -----------------------------------------------------------------------------

    if (req.body.bairro) {
      console.log("4");

      try {
        const resultsJustWithMovies = await Sessoes.findAll({
          where: {
            bairro: req.body.bairro,
            cineminha: req.params.id,
          },
        });

        if (resultsJustWithMovies.length !== 0) {
          return res.status(200).json(resultsJustWithMovies);
        }
      } catch (error) {
        console.log(error);
      }
    } // ----------------------------------------------------------

    if (req.body.cidade) {
      console.log("4");

      try {
        const resultsJustWithMovies = await Sessoes.findAll({
          where: {
            cidade: req.body.cidade,
            cineminha: req.params.id,
          },
        });

        if (resultsJustWithMovies.length !== 0) {
          return res.status(200).json(resultsJustWithMovies);
        }
      } catch (error) {
        console.log(error);
      }
    }

    // ---------------------------------------------------------------

    if (req.params.id) {
      console.log("4");

      try {
        const resultsJustWithId = await Sessoes.findAll({
          where: {
            cineminha: req.params.id,
          },
        });

        if (resultsJustWithId.length !== 0) {
          return res.status(200).json(resultsJustWithId);
        }
      } catch (error) {
        console.log(error);
      }
    }

    return res.status(400).json({ error: "nenhum filme achado na pesquisa" });
  },
};