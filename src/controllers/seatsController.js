const { Filmes, Sessoes, Assentos ,Usuarios } = require("../models/cineMainModel");

module.exports = {
    getSeat: async(req,res) => {
        try {
            const assento = await Assentos.findOne({
                where: {
                    sessionId: req.params.sessionId,
                    id: req.params.seatId
                }
            })

            if (assento){
                res.status(200).json(assento)
            }else{
                res.status(400).json({error: "Essa sessão não tem esse assento!"})
            }

        } catch (error) {
            
        }
    },
    getAllSeats: async(req,res) => {
        try {
            console.log("sefesf")
            const assentos = await Assentos.findAll({
                where: {
                    sessionId: req.params.seats,
                }
            })
            return res.status(200).json(assentos);
        } catch (error) {
            return res.status(500).json(error);
        }
    },
    patchSeat: async(req,res) => {
        try {
            const assento = await Assentos.findOne({
                where: {
                    sessionId: req.params.sessionId,
                    id: req.params.seatId
                }
            })

            const assentoUpdate = await assento.update({
                cpfOcupante: req.body.cpfOcupante,
                nomeOcupante: req.body.nomeOcupante,
                tipoDoAssento: req.body.tipoDoAssento,
                
            })

            return res.status(200).json(assentoUpdate);
        } catch (error) {
            res.status(400).json({error: "Erro ao modificar o assento"})
        }
    }
}