const { sequelize } = require("../connection/createConfigData");
const { Filmes, Sessoes, Usuarios } = require("../models/cineMainModel");
const { Op } = require('sequelize');

module.exports = {
  postUser: async (req, res) => {
    try {
      const senha =  req.body.senha;
      const confirmarSenha =  req.body.confirmarSenha;

      if(senha != confirmarSenha) {
        return res.status(400).json({error: "As senhas não conferem!"})
      }

      const username = await Usuarios.findAll({
        where:{
            email: req.body.email
        },
      });

      if(username != 0){
        return res.status(400).json({error: "Email já cadastrado"});
      }
      const user = await Usuarios.create({
        nome: req.body.nome,
        sobrenome: req.body.sobrenome,
        cpf: req.body.cpf,
        dataDeNascimento: req.body.dataDeNascimento,
        nomeDeUsuario: req.body.nomeDeUsuario,
        email: req.body.email,
        senha: req.body.senha,
        confirmarSenha: req.body.confirmarSenha,
      });
      
      res.status(201).json(user);
    } catch (error) {
      res.status(500).json(error);
    }
  },

  validateUser: async(req, res) => {
    try {
      
    const user1 = await Usuarios.findAll({  
      where:{
        nomeDeUsuario: req.body.login,
        senha: req.body.senha
      }    
    })
    
    const user2 = await Usuarios.findAll({
      where: {
        email: req.body.login,
        senha: req.body.senha
      }
    })

    console.log(user1, user2)

    if (user1.length > user2.length){
      console.log("entrou user1 com username")

      user1.logou = true

      return res.json(user1)
    }else if(user1.length < user2.length){  
      console.log("entrou user1 com email")
      user2.logou = true

      return res.json(user2)
    }

    return res.status(400).json(error);

    } catch(error){
      return res.status(500).json(error);
    }
  },
  
  allUsers: async (req,res)=>{
    try {
      const users = await Usuarios.findAll();
      return res.json(users)
    } catch (error) {
      return res.status(500).json(error);
    }
  },
  findUser: async (req,res)=>{
    try {
      const user = await Usuarios.findAll({
        where: {
          nome: req.params.nome
        }
      });
      return res.json(user)
    } catch (error) {
      return res.status(500).json(error);
    }
  }
}