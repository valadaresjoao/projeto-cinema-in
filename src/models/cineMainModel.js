const { sequelize } = require("../connection/createConfigData");
const { DataTypes } = require("sequelize");
const { v4: uuidv4 } = require("uuid");

const Filmes = sequelize.define("Filmes", {
  titulo: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  direcao: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  urlImagem: {
    type: DataTypes.STRING,
    allowNull: false,
    validate: {
      isUrl: true,
    },
  },
  sinopse: {
    type: DataTypes.STRING,
    allowNull: false,
    validate: {
      len: [1, 140]
    }
  },
  genero: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  classificacao: {
    type: DataTypes.INTEGER,
    allowNull: false,
    validate: {
      min: 0,
      max: 5,
      isNumeric: {
        msg: "Somente é aceito números",
      },
    },
  },
  id: {
    type: DataTypes.UUIDV4,
    allowNull: false,
    primaryKey: true,
    defaultValue: () => uuidv4(),
  },
});

const Sessoes = sequelize.define("sessoes", {
  // No horário estou com dúvida, diz que o formato deve ser de (hh/mm) mas esses valores são ser dados no body da req e não na criação do modelo eu suponho
  horario: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  cidade: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  bairro: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  tipo: {
    type: DataTypes.INTEGER,
    allowNull: false,
    validate: {
        min: 0,
        max: 2
    },
  },
  id: {
    type: DataTypes.UUIDV4,
    primaryKey: true,
    allowNull: false,
    defaultValue: ()=>uuidv4()
  },
});

const Assentos = sequelize.define("assentos", {
    numero: {
        type: DataTypes.INTEGER,
        allowNull: false,
        define:{
            min: 1,
            max: 18,
        }
    },
    fileira: {
        type: DataTypes.INTEGER,
        allowNull: false,
        define: {
            min: 1,
            max: 10,
            }
        },
    preco: {
      type: DataTypes.FLOAT,
      allowNull: false,
      validate: {
        isNumeric: {
            msg: "Só é aceito números"
        }, 
      }
    },
    cpfOcupante: {
        type: DataTypes.STRING,
        allowNull: true,
        validate: {
          is: ["[0-9]"],
          len: 11,              // only allow values with length between 2 and 10
        }
    },
    nomeOcupante: {
      type: DataTypes.STRING,
      allowNull: true
    },
    tipoDoAssento: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0,
      validate: {
          min: 0,
          max: 2
      }
    },
    id: {
      type: DataTypes.UUIDV4,
      allowNull: false,
      primaryKey: true,
      defaultValue: ()=>uuidv4()
    },
    selecionado: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    comprado: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
})

const Usuarios = sequelize.define("usuarios", {
  nome: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  sobrenome: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  cpf: {
    type: DataTypes.STRING,
    allowNull: false,
    validate: {
      is: ["[0-9]"],
      len: 11, // only allow values with length between 2 and 10
    },
  },
  dataDeNascimento: {
    type: DataTypes.STRING,
    allowNull: false,
    validate: {
      isDate: {
        msg: "Necessário formato de data [ANO-MÊS-DIA]",
      },
    },
  },
  nomeDeUsuario: {
    type: DataTypes.STRING,
    allowNull: false,
    unique: true,
  },
  email: {
    type: DataTypes.STRING,
    allowNull: false,
    validate: {
      isEmail: true,
    },
  },
  senha: {
    type: DataTypes.STRING,
    allowNull: false,
    validate: {
      is: ["[!@#$%^&*]"],
      len: 7
    }
  },
  id: {
    type: DataTypes.UUIDV4,
    allowNull: false,
    primaryKey: true,
    defaultValue: () => uuidv4(),
  },

  logou: {
    type: DataTypes.BOOLEAN,
    allowNull: true,
    defaultValue: false
  }

});

// Digo a correlação entre as tabelas
Filmes.hasMany(Sessoes, {onDelete: "CASCADE"})
Sessoes.belongsTo(Filmes, {
  constraints: true,
  foreignKey: "cineminha"
})

Sessoes.hasMany(Assentos, {onDelete: "CASCADE"})
Assentos.belongsTo(Sessoes, {
  constraints: true,
  foreignKey: "sessionId"
})

module.exports = {
  Filmes,
  Sessoes,
  Assentos,
  Usuarios,
};
